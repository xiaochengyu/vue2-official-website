# official_website

#### 介绍
official_website项目是一套基于现代web开发技术栈的在线官网系统。
前端采用响应式设计，确保在各种设备上的良好展示效果；后端采用Spring Boot等技术，确保系统的高性能和稳定性。
通过这套官网系统，探囊取莓有限公司可以更好地展示其产品和服务，提升客户满意度，推动业务持续发展。

#### 软件架构
official_website项目采用了现代化的Web开发技术栈，结合多层架构和设计模式，确保系统的高性能、可维护性和可扩展性。

 **前端** 

- 框架：使用Vue.js作为前端框架，结合Vue Router进行页面导航，Vuex进行状态管理。
- 设计模式：组件化开发，通过将页面分解为多个独立的组件，提高代码的复用性和可维护性。

 **后端** 


- 框架：使用Spring Boot作为后端框架，提供快速构建和配置简化的能力。
- 设计模式：

 


    1. MVC模式：分离数据模型（Model）、视图（View）和控制器（Controller），提高代码的可维护性。
    1. 依赖注入（DI）：通过Spring的依赖注入机制，降低组件之间的耦合度。
    1. RESTful API：使用Spring MVC构建RESTful API，为前端提供数据服务。
    1. DAO模式：使用Spring Data JPA进行数据持久化操作，简化数据库访问层的代码。
    1. 服务层模式：在Controller和DAO之间添加服务层（Service Layer），处理业务逻辑，确保代码结构清晰。



 **数据库** 

- 数据库：使用MySQL作为关系型数据库，存储系统的核心数据。
- 数据库访问：使用Spring Data JPA进行数据库操作，简化数据访问层的实现。

 **服务器** 
- 服务器：使用Tomcat作为应用服务器，部署和运行Spring Boot应用。

### 网页展示


![输入图片说明](FireShot%20Capture%20001%20-%20%E6%8E%A2%E5%9B%8A%E5%8F%96%E8%8E%93%E6%9C%89%E9%99%90%E5%85%AC%E5%8F%B8%20-%20localhost.png)
![输入图片说明](FireShot/FireShot%20Capture%20002%20-%20%E6%8E%A2%E5%9B%8A%E5%8F%96%E8%8E%93%E6%9C%89%E9%99%90%E5%85%AC%E5%8F%B8%20-%20localhost.png)
![输入图片说明](FireShot/FireShot%20Capture%20003%20-%20%E6%8E%A2%E5%9B%8A%E5%8F%96%E8%8E%93%E6%9C%89%E9%99%90%E5%85%AC%E5%8F%B8%20-%20localhost.png)
![输入图片说明](FireShot/FireShot%20Capture%20004%20-%20%E6%8E%A2%E5%9B%8A%E5%8F%96%E8%8E%93%E6%9C%89%E9%99%90%E5%85%AC%E5%8F%B8%20-%20localhost.png)