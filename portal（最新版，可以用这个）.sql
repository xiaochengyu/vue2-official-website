/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 80025
 Source Host           : localhost:3306
 Source Schema         : portal

 Target Server Type    : MySQL
 Target Server Version : 80025
 File Encoding         : 65001

 Date: 05/07/2024 17:28:14
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for t_article
-- ----------------------------
DROP TABLE IF EXISTS `t_article`;
CREATE TABLE `t_article`  (
  `article_id` bigint(0) NOT NULL AUTO_INCREMENT COMMENT '文章id',
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '标题',
  `cover` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '封面',
  `introduction` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '文章介绍',
  `content` text CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL COMMENT '内容',
  `content_html` text CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL COMMENT '内容html',
  `author` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '文章作者',
  `create_time` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '发布时间',
  `is_delete` int(0) NULL DEFAULT 0 COMMENT '逻辑删除',
  PRIMARY KEY (`article_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 10 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_article
-- ----------------------------
INSERT INTO `t_article` VALUES (1, '全国仿真创新应用大赛', 'https://wx1.sinaimg.cn/mw690/005Sy8A1ly1hdz8pxn7kzj30sa0k0wji.jpg', '山西省二等奖', '电梯会自动停止运行，没有危险。如果供电部门有计划停电，电梯会提前通知或停止运行。\r\n\r\n如果电梯在运行过程中突然停电或供电线路出现故障，电梯将自动停止运行，无危险。由于电梯本身配备了电气和机械安全装置，一旦停电，电梯制动器将自动制动，使电梯停止运行，此外，如果供电部门有计划停电，将提前通知电梯或提前停止运行。当电梯停电时，汽车下部有一个安全钳，它会卡住导轨，使汽车固定在导轨上，不会滑动。电梯有一个汽车和一个对重，通过钢丝绳连接，钢丝绳由驱动装置（牵引机）的牵引驱动，使电梯汽车和对重在电梯导轨上上上下移动。\r\n\r\n\r\n\r\n国家对电梯使用的钢丝绳电梯有特殊规定和要求。钢丝绳的配置不仅是为了承受电梯轿厢和额定载荷，还考虑了牵引力的大小。因此，钢丝绳的抗拉强度大大于电梯的载荷重量，其安全系数超过12。一般电梯配备四根以上钢丝绳，一次电梯钢丝绳不会断裂。如果电梯在运行过程中突然停电或供电线路出现故障，电梯将自动停止运行，无危险。由于电梯本身配备了电气和机械安全装置，一旦停电，电体的起动器将自动制动，使电梯无法运行。此外，如果供电部门有计划停电，将提前通知电梯或提前停止运行。电梯的运行速度，无论是上下，都应在规定的额定速度范围内运行，一般不超速。如果超速，电梯控制系统中有防超速装置。此时，该装置将自动减速或停止运行。', '<h5>职位描述：</h5>\r\n													<p>\r\n														熟练应用各种市场分析工具及产品原型工具，能够撰写和输出规范的产品白皮书；\r\n													</p>\r\n													<p>\r\n														有产品策划和产品管理思维，能够撰写市场调研、市场分析、可行性分析报告等；\r\n													</p>\r\n													<p>\r\n														具备良好的沟通协调能力、系统性思维和创新意识；\r\n													</p>\r\n													<p>\r\n														具有产品需求、归纳能力、市场敏觉洞察能力；\r\n													</p>\r\n													<p>\r\n														经历过完整的企业级应用产品生命周期；\r\n													</p>\r\n													<p>\r\n														有TOB企业5年以上产品经理工作经验；\r\n													</p>\r\n													<p>\r\n														有前端相关技术背景，具有产品架构师能力者优先考虑。\r\n													</p>\r\n													\r\n													<h5>请将您的简历发送至： <a href=\"mailto:jobs@XXX\">jobs@XXX</a></h5>\r\n<h5>职位描述：</h5>\r\n													<p>\r\n														熟练应用各种市场分析工具及产品原型工具，能够撰写和输出规范的产品白皮书；\r\n													</p>\r\n													<p>\r\n														有产品策划和产品管理思维，能够撰写市场调研、市场分析、可行性分析报告等；\r\n													</p>\r\n													<p>\r\n														具备良好的沟通协调能力、系统性思维和创新意识；\r\n													</p>\r\n													<p>\r\n														具有产品需求、归纳能力、市场敏觉洞察能力；\r\n													</p>\r\n													<p>\r\n														经历过完整的企业级应用产品生命周期；\r\n													</p>\r\n													<p>\r\n														有TOB企业5年以上产品经理工作经验；\r\n													</p>\r\n													<p>\r\n														有前端相关技术背景，具有产品架构师能力者优先考虑。\r\n													</p>\r\n													\r\n													<h5>请将您的简历发送至： <a href=\"mailto:jobs@XXX\">jobs@XXX</a></h5>', '张三', '2023-01-19 12:30:44', 0);
INSERT INTO `t_article` VALUES (2, '机械产品数字化设计大赛', 'https://wx3.sinaimg.cn/mw690/005Sy8A1ly1hdz8pxxkovj30p90k0jtg.jpg', '校二等奖', '现在每个人的生活水平都在不断提高，越来越多的人住在别墅里。住在别墅后，需要去三楼和四楼甚至更高层是很常见的。此时，别墅家用电梯的使用越来越普遍。家用电梯的种类和型号仍然很多。购买电梯时，需要根据具体情况选择合适的电梯尺寸和承重比例。家用电梯预留尺寸说明中有相关说明。在选择时，您可以进行比较，看看它有多大，尺寸正好，以满足需求。根据不同的分类，家用电梯的结构不同，结构也决定了尺寸。家用电梯的选择、不同的配置和尺寸也有很大的选择空间。我们应该分析具体情况。\r\n\r\n\r\n\r\n家用电梯分为液压家用电梯、无机房牵引家用电梯和进口螺旋家用电梯。电梯结构由井道和坑、升降平台或轿厢、层站等组成。层站越多，尺寸和高度就越高。\r\n\r\n\r\n\r\n一、观光别墅电梯，最小预留尺寸：1200mm*1380mm。观光别墅电梯不仅可以安装在室内，也可以安装在室外，空间较少，大大提高了空间利用率。\r\n\r\n\r\n\r\n二、大型重型别墅电梯，最小预留尺寸：1565mm*2320mm。大型载重别墅电梯空间大，便于我们携带一些体积大、不易移动的物品。\r\n\r\n\r\n\r\n三、优雅轿厢别墅电梯，最小预留尺寸：1500mm*1760mm。优雅的轿厢别墅电梯内部优雅宽敞，占用空间小，安全舒适并存，是传统电梯无法比拟的。\r\n\r\n', '<h5>职位描述：</h5>\r\n													<p>\r\n														熟练应用各种市场分析工具及产品原型工具，能够撰写和输出规范的产品白皮书；\r\n													</p>\r\n													<p>\r\n														有产品策划和产品管理思维，能够撰写市场调研、市场分析、可行性分析报告等；\r\n													</p>\r\n													<p>\r\n														具备良好的沟通协调能力、系统性思维和创新意识；\r\n													</p>\r\n													<p>\r\n														具有产品需求、归纳能力、市场敏觉洞察能力；\r\n													</p>\r\n													<p>\r\n														经历过完整的企业级应用产品生命周期；\r\n													</p>\r\n													<p>\r\n														有TOB企业5年以上产品经理工作经验；\r\n													</p>\r\n													<p>\r\n														有前端相关技术背景，具有产品架构师能力者优先考虑。\r\n													</p>\r\n													\r\n													<h5>请将您的简历发送至： <a href=\"mailto:jobs@XXX\">jobs@XXX</a></h5>\r\n<h5>职位描述：</h5>\r\n													<p>\r\n														熟练应用各种市场分析工具及产品原型工具，能够撰写和输出规范的产品白皮书；\r\n													</p>\r\n													<p>\r\n														有产品策划和产品管理思维，能够撰写市场调研、市场分析、可行性分析报告等；\r\n													</p>\r\n													<p>\r\n														具备良好的沟通协调能力、系统性思维和创新意识；\r\n													</p>\r\n													<p>\r\n														具有产品需求、归纳能力、市场敏觉洞察能力；\r\n													</p>\r\n													<p>\r\n														经历过完整的企业级应用产品生命周期；\r\n													</p>\r\n													<p>\r\n														有TOB企业5年以上产品经理工作经验；\r\n													</p>\r\n													<p>\r\n														有前端相关技术背景，具有产品架构师能力者优先考虑。\r\n													</p>\r\n													\r\n													<h5>请将您的简历发送至： <a href=\"mailto:jobs@XXX\">jobs@XXX</a></h5>', '李四', '2023-01-19 12:30:44', 0);
INSERT INTO `t_article` VALUES (3, '专利申请受理通知书', 'https://wx4.sinaimg.cn/mw690/005Sy8A1ly1hdz8py95ghj30s2152qc1.jpg', '正在授权中', '朋友们一定都坐过电梯，但你知道他的各个部分是怎么工作的吗？电梯可分为以下部分：\r\n\r\n\r\n\r\n电气控制系统：电气系统控制电梯运动，完成层选择、平面、速度测量和照明工作。指示呼叫系统随时显示轿厢的运动方向和地板位置。轿厢和对重：轿厢是承载乘客或其他负荷的箱体部件。对重用于平衡轿厢负荷，降低电机功率。轿厢和对重主要通过钢丝绳连接。除钢丝绳外，还有连接轿厢和对重的补偿装置。补偿装置用于补偿运动中钢丝绳张力和重量的变化，稳定牵引电机负载，准确停车。\r\n\r\n\r\n\r\n钢丝绳：钢丝绳两端连接轿厢和对重，缠绕在牵引轮和导轮上，即牵引比为1：1。或者两端分别固定，钢丝绳分为两部分，一部分通过滑轮中间，另一部分通过滑轮中间。即牵引比为2：1。\r\n\r\n\r\n\r\n牵引机：当永磁同步电机时，牵引机直接与牵引轮同步旋转。当异步电机时，牵引机驱动牵引轮通过减速器旋转，牵引绳与牵引轮摩擦产生的牵引力实现汽车和重型升降运动，达到运输目的。\r\n\r\n\r\n\r\n主机闸门：电机工作时松开闸门，取消主机运行阻力，确保电梯正常运行，突然停电时制动，停止轿厢运动。在电梯正常状态下，当层站停靠时开关门时，闸门也断电，保持轿厢静止，供人员和货物进出。导轨和导靴：固定在轿厢上的导靴可沿安装在建筑井道墙上的固定导轨往复提升，防止轿厢在运行中倾斜或摆动。\r\n\r\n电梯有轿厢和对重，通过钢丝绳连接。钢丝绳由驱动装置（牵引机）的牵引驱动，使电梯轿厢和对重在电梯内导轨上上下移动。\r\n\r\n\r\n\r\n牵引绳的两端分别与轿厢和对重相连，缠绕在牵引轮和导向轮上。牵引电机通过减速器变速后驱动牵引轮旋转，通过牵引绳与牵引轮摩擦产生的牵引力，实现轿厢和对重的升降运动，达到运输目的。固定在轿厢上的导靴可沿安装在建筑井道墙上的固定导轨来回升降，防止轿厢在运行过程中倾斜或摆动。电机工作时，经常关闭块式制动器松开闸门，使电梯运行，在失电时制动，使轿厢停止升降，并在指定层站上保持其静止状态，供人员和货物进出。轿厢是运载乘客或其他载荷的箱体部件，对重用于平衡轿厢载荷，降低电机功率。补偿装置用于补偿牵引绳运动中的张力和重量变化，使牵引电机负载稳定，使轿厢能够准确停放。电气系统可控制电梯运动，同时完成选层、平层、测速、照明工作。指示电话系统随时显示轿厢的运动方向和地板位置。确保电梯的安全。', '<h5>职位描述：</h5>\r\n													<p>\r\n														熟练应用各种市场分析工具及产品原型工具，能够撰写和输出规范的产品白皮书；\r\n													</p>\r\n													<p>\r\n														有产品策划和产品管理思维，能够撰写市场调研、市场分析、可行性分析报告等；\r\n													</p>\r\n													<p>\r\n														具备良好的沟通协调能力、系统性思维和创新意识；\r\n													</p>\r\n													<p>\r\n														具有产品需求、归纳能力、市场敏觉洞察能力；\r\n													</p>\r\n													<p>\r\n														经历过完整的企业级应用产品生命周期；\r\n													</p>\r\n													<p>\r\n														有TOB企业5年以上产品经理工作经验；\r\n													</p>\r\n													<p>\r\n														有前端相关技术背景，具有产品架构师能力者优先考虑。\r\n													</p>\r\n													\r\n													<h5>请将您的简历发送至： <a href=\"mailto:jobs@XXX\">jobs@XXX</a></h5>\r\n<h5>职位描述：</h5>\r\n													<p>\r\n														熟练应用各种市场分析工具及产品原型工具，能够撰写和输出规范的产品白皮书；\r\n													</p>\r\n													<p>\r\n														有产品策划和产品管理思维，能够撰写市场调研、市场分析、可行性分析报告等；\r\n													</p>\r\n													<p>\r\n														具备良好的沟通协调能力、系统性思维和创新意识；\r\n													</p>\r\n													<p>\r\n														具有产品需求、归纳能力、市场敏觉洞察能力；\r\n													</p>\r\n													<p>\r\n														经历过完整的企业级应用产品生命周期；\r\n													</p>\r\n													<p>\r\n														有TOB企业5年以上产品经理工作经验；\r\n													</p>\r\n													<p>\r\n														有前端相关技术背景，具有产品架构师能力者优先考虑。\r\n													</p>\r\n													\r\n													<h5>请将您的简历发送至： <a href=\"mailto:jobs@XXX\">jobs@XXX</a></h5>', '王五', '2023-01-19 12:30:44', 0);

-- ----------------------------
-- Table structure for t_company
-- ----------------------------
DROP TABLE IF EXISTS `t_company`;
CREATE TABLE `t_company`  (
  `company_id` int(0) NOT NULL AUTO_INCREMENT COMMENT '公司id',
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '名称',
  `introduction` text CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL COMMENT '公司介绍',
  `phone` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '电话号码',
  `address` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '地址',
  `qr_code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '二维码',
  `create_time` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '创建时间',
  `is_delete` int(0) NULL DEFAULT 0 COMMENT '逻辑删除',
  PRIMARY KEY (`company_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_company
-- ----------------------------
INSERT INTO `t_company` VALUES (1, 'Berry团队', '我们团队的名字叫Berry团队。为响应国家《农机装备发展行动方案（2016—2025）》、《关于加快推进农业机械化和农机装备产业转型升级的指导意见》、《关于加快推进农业机械化转型升级通知》等政策的号召，开发出草莓机器人。我们的口号是“科技采摘，健康无害”。 \n本团队在教授的指导下，由本团队内部人员协作研发而成。本项目由团队自主研发，目前项目核心专利已受理。', '15312833321', '山西省 太原市 万柏林区', NULL, NULL, 0);

-- ----------------------------
-- Table structure for t_designer
-- ----------------------------
DROP TABLE IF EXISTS `t_designer`;
CREATE TABLE `t_designer`  (
  `designer_id` int(0) NOT NULL AUTO_INCREMENT COMMENT '设计师id',
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '名字',
  `avatar` varchar(600) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '头像',
  `content` text CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL COMMENT '内容',
  `content_html` text CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL COMMENT '内容html',
  `create_time` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '发布时间',
  `is_delete` int(0) NULL DEFAULT 0 COMMENT '逻辑删除',
  PRIMARY KEY (`designer_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 12 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_designer
-- ----------------------------
INSERT INTO `t_designer` VALUES (1, '宋景诗', 'https://wx3.sinaimg.cn/mw690/005Sy8A1ly1he3swmqr2gj32qf3nhkjl.jpg', '软件工程', NULL, '2020-10-03', 0);
INSERT INTO `t_designer` VALUES (2, '张玉琪', 'https://wx2.sinaimg.cn/mw690/005Sy8A1ly1he3swnenmcj30fv0m80t2.jpg', '物联网工程', NULL, '2020-10-03', 0);
INSERT INTO `t_designer` VALUES (3, '李敬', 'https://wx4.sinaimg.cn/mw690/005Sy8A1ly1he3swkhv0oj30bn0g9403.jpg', '机自专业', NULL, '2020-10-03', 0);
INSERT INTO `t_designer` VALUES (4, '刘峰焱', 'https://wx4.sinaimg.cn/mw690/005Sy8A1ly1he3swo44d4j30py0zk76o.jpg', '工业设计 ', NULL, '2020-10-03', 0);
INSERT INTO `t_designer` VALUES (5, '任书玮', 'https://wx4.sinaimg.cn/mw690/005Sy8A1ly1he3swkue4pj30sy0yoq8s.jpg', '数据计算', NULL, '2020-10-03', 0);
INSERT INTO `t_designer` VALUES (6, '杨鹏', 'https://wx3.sinaimg.cn/mw690/005Sy8A1ly1he3swl7oulj30m50uzgmm.jpg', '机自专业', NULL, '2020-10-03', 0);
INSERT INTO `t_designer` VALUES (7, '贾雅萌', 'https://wx1.sinaimg.cn/mw690/005Sy8A1ly1he3swnl4zuj30fg0m8glr.jpg', '会计专业', NULL, NULL, 0);
INSERT INTO `t_designer` VALUES (8, '崔津华', 'https://wx4.sinaimg.cn/mw690/005Sy8A1ly1he3swl1jmhj30ku0t6wfe.jpg', '机器人专业', NULL, NULL, 0);
INSERT INTO `t_designer` VALUES (10, '王海睿', 'https://wx1.sinaimg.cn/mw690/005Sy8A1ly1he3swledyuj30870bhgln.jpg', '机电专业', NULL, NULL, 0);
INSERT INTO `t_designer` VALUES (11, '徐恩赐', 'https://wx3.sinaimg.cn/mw690/005Sy8A1ly1he3swn5o7dj30u018z44o.jpg', '市场营销', NULL, NULL, 0);
INSERT INTO `t_designer` VALUES (12, '司梦婕', 'https://wx4.sinaimg.cn/mw690/005Sy8A1ly1he3swnwa14j318t1v7n48.jpg', '视觉传媒', NULL, NULL, 0);

-- ----------------------------
-- Table structure for t_example
-- ----------------------------
DROP TABLE IF EXISTS `t_example`;
CREATE TABLE `t_example`  (
  `example_id` bigint(0) NOT NULL AUTO_INCREMENT COMMENT '案例id',
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '案例名称',
  `example_type` int(0) NULL DEFAULT NULL COMMENT '案例种类（0，1，2，3，4，5）',
  `image1` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '案例图片(1-16张)',
  `image2` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `image3` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `image4` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `image5` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `image6` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `image7` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `image8` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `image9` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `image10` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `image11` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `image12` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `image13` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `image14` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `image15` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `image16` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `content` text CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL COMMENT '内容',
  `content_html` text CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL COMMENT '内容html',
  `create_time` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '创建时间',
  `is_delete` int(0) NULL DEFAULT 0 COMMENT '逻辑删除',
  PRIMARY KEY (`example_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_example
-- ----------------------------
INSERT INTO `t_example` VALUES (1, '成都雅居乐花园安装家用电梯', 0, 'https://wx3.sinaimg.cn/mw690/005Sy8A1ly1hdz7x7w63rj30zk0hfgoa.jpg', 'https://wx2.sinaimg.cn/mw690/005Sy8A1ly1hdz7x82ehtj30zk0h7acb.jpg', 'https://wx2.sinaimg.cn/mw690/005Sy8A1ly1hdz7x8bwv9j310q0ei7cg.jpg', 'https://wx3.sinaimg.cn/mw690/005Sy8A1ly1hdz7x8k1l6j30ze08w78u.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '成都别墅家用电梯\r\n别墅家用电梯（小机房）\r\n\r\n都市建筑优选解决方案\r\n\r\n成都奥斯别墅小机房电梯的推出，引领了电梯行业从此趋向于小机房设备配置，小机房电梯既满足客运、也可以用于货运；适用于普通办公室、酒店、住宅楼、甚至是摩天大厦等各种类型的建筑。\r\n\r\n1、节省30%的机房面积\r\n\r\n2、便捷安装 提高效率\r\n\r\n3、降低成本\r\n\r\n4、高效节能 \r\n\r\n家用电梯（无机房）\r\n\r\n随着技术的进步和发展，亚洲富士电梯以深谙环保理念、节能降耗、节约建筑面积为己任，推出了亚洲富士无机房电梯，该款电梯只需要一个独立的井道空间，无需机房。\r\n\r\n 1、减少40%的电梯能耗\r\n\r\n2、节约10%的建筑面积', NULL, '2020-10-03', 0);
INSERT INTO `t_example` VALUES (2, '成都麓山国际家用电梯安装展示效果', 1, 'https://wx4.sinaimg.cn/mw690/005Sy8A1ly1hdz7763kk8j30zc0ouq7j.jpg', 'https://wx4.sinaimg.cn/mw690/005Sy8A1ly1hdz776bw04j30zi0m8n34.jpg', 'https://wx4.sinaimg.cn/mw690/005Sy8A1ly1hdz776lqkmj30zs0psjyf.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '成都麓山国际别墅电梯安装展示效果（只有图片没得内容）', NULL, '2020-10-03', 0);
INSERT INTO `t_example` VALUES (3, '成都别墅家用电梯各种装修风格展示', 2, 'https://www.alsidt.com/public/uploads/20201003/880fdc8f430da010a1e91c68fa4440d8.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '根据不同风格、不同结构的别墅，灵巧设计，空间利用率高，使电梯完全融入别墅之中，打造极具美学品质的电梯空间和人性化乘梯体验。可无土建井道，量身定做钢结构井道，省时省力。电压可低至220V，耗电量低，低碳环保节能。自主选择符合自己审美观的别墅梯轿厢、厅门样式，完全按照自己的想法设计外观，适合各种装修风格。为私人休闲生活增添方便实用、舒心的乐趣。', NULL, '2020-10-03', 0);
INSERT INTO `t_example` VALUES (4, '成都别墅家用电梯各种装修风格展示', 3, 'https://www.alsidt.com/public/uploads/20201003/87fe6fb24f1dca3319c0ce8d5132439c.jpg', 'https://www.alsidt.com/public/uploads/20201003/87fe6fb24f1dca3319c0ce8d5132439c.jpg', 'https://www.alsidt.com/public/uploads/20201003/87fe6fb24f1dca3319c0ce8d5132439c.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0);
INSERT INTO `t_example` VALUES (5, '成都别墅家用电梯各种装修风格展示', 4, 'https://www.alsidt.com/public/uploads/20201003/87fe6fb24f1dca3319c0ce8d5132439c.jpg', 'https://www.alsidt.com/public/uploads/20201003/87fe6fb24f1dca3319c0ce8d5132439c.jpg', 'https://www.alsidt.com/public/uploads/20201003/87fe6fb24f1dca3319c0ce8d5132439c.jpg', 'https://www.alsidt.com/public/uploads/20201003/87fe6fb24f1dca3319c0ce8d5132439c.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0);
INSERT INTO `t_example` VALUES (6, '成都别墅家用电梯各种装修风格展示', 5, 'https://www.alsidt.com/public/uploads/20201003/87fe6fb24f1dca3319c0ce8d5132439c.jpg', 'https://www.alsidt.com/public/uploads/20201003/87fe6fb24f1dca3319c0ce8d5132439c.jpg', 'https://www.alsidt.com/public/uploads/20201003/87fe6fb24f1dca3319c0ce8d5132439c.jpg', 'https://www.alsidt.com/public/uploads/20201003/87fe6fb24f1dca3319c0ce8d5132439c.jpg', 'https://www.alsidt.com/public/uploads/20201003/87fe6fb24f1dca3319c0ce8d5132439c.jpg', 'https://www.alsidt.com/public/uploads/20201003/87fe6fb24f1dca3319c0ce8d5132439c.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0);

-- ----------------------------
-- Table structure for t_slideshow
-- ----------------------------
DROP TABLE IF EXISTS `t_slideshow`;
CREATE TABLE `t_slideshow`  (
  `slideshow_id` int(0) NOT NULL AUTO_INCREMENT COMMENT '轮播图id',
  `image_url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '图片地址',
  `introduction` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '描述',
  `create_time` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '创建时间',
  `is_delete` int(0) NULL DEFAULT 0 COMMENT '逻辑删除',
  PRIMARY KEY (`slideshow_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_slideshow
-- ----------------------------
INSERT INTO `t_slideshow` VALUES (1, 'https://p8.itc.cn/q_70/images03/20210711/f3c32588e8b046b98ac6ff82b91d4d63.jpeg', NULL, NULL, 0);
INSERT INTO `t_slideshow` VALUES (2, 'https://p8.itc.cn/q_70/images03/20210711/f3c32588e8b046b98ac6ff82b91d4d63.jpeg', NULL, NULL, 0);
INSERT INTO `t_slideshow` VALUES (3, 'https://p8.itc.cn/q_70/images03/20210711/f3c32588e8b046b98ac6ff82b91d4d63.jpeg', NULL, NULL, 0);
INSERT INTO `t_slideshow` VALUES (4, 'https://p8.itc.cn/q_70/images03/20210711/f3c32588e8b046b98ac6ff82b91d4d63.jpeg', NULL, NULL, 0);

-- ----------------------------
-- Table structure for t_type
-- ----------------------------
DROP TABLE IF EXISTS `t_type`;
CREATE TABLE `t_type`  (
  `type_id` int(0) NOT NULL AUTO_INCREMENT COMMENT '电梯种类id',
  `type_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '0' COMMENT '电梯种类名称',
  `is_delete` int(0) NULL DEFAULT 0 COMMENT '逻辑删除',
  `create_time` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`type_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_type
-- ----------------------------
INSERT INTO `t_type` VALUES (1, '采摘板块', 0, '2020-10-03 ');
INSERT INTO `t_type` VALUES (2, '收纳板块', 0, '2020-10-03 ');
INSERT INTO `t_type` VALUES (3, '移动板块', 0, '2020-10-03 ');

-- ----------------------------
-- Table structure for t_type_details
-- ----------------------------
DROP TABLE IF EXISTS `t_type_details`;
CREATE TABLE `t_type_details`  (
  `type_detais_id` int(0) NOT NULL AUTO_INCREMENT COMMENT '种类详情id',
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '标题',
  `image_url1` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '图片',
  `image_url2` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `image_url3` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `image_url4` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `image_url5` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `type_id` int(0) NULL DEFAULT NULL COMMENT '产品类型id',
  `introduction` text CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL COMMENT '描述内容',
  `create_time` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '创建时间',
  `is_delete` int(0) NULL DEFAULT 0 COMMENT '逻辑删除',
  PRIMARY KEY (`type_detais_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_type_details
-- ----------------------------
INSERT INTO `t_type_details` VALUES (1, '采摘机械臂正面图', 'https://wx4.sinaimg.cn/mw690/005Sy8A1ly1hdz7e9jvjhj31421g3afj.jpg', 'https://www.alsidt.com/public/uploads/20201003/415c5ac4bef61cdc4b647a3d15ddaafd.jpg', 'https://www.alsidt.com/public/uploads/20201003/ead8f6adb992833dc6f6b7bf8f1bcdad.jpg', 'https://www.alsidt.com/public/uploads/20201003/ead8f6adb992833dc6f6b7bf8f1bcdad.jpg', 'https://www.alsidt.com/public/uploads/20220331/e83d8c0d46f6bfc580b3642775c22dd9.png', 1, '成都蔚蓝卡地亚家用别墅电梯', '2020-10-03', 0);
INSERT INTO `t_type_details` VALUES (2, '机械臂数据图', 'https://wx2.sinaimg.cn/mw690/005Sy8A1ly1hdz7gl5l8pj30qa1g3go8.jpg', 'https://www.alsidt.com/public/uploads/20201003/42835265b591791c3f5e15917f437763.jpg', 'https://www.alsidt.com/public/uploads/20201003/45fa74a8ccbdaead063c99a9537f2d2d.jpg', 'https://www.alsidt.com/public/uploads/20201003/58fe9951ce28b923977ffc697e2ded25.jpg', NULL, 1, '成都雅居乐花园安装家用电梯', '2020-10-03', 0);
INSERT INTO `t_type_details` VALUES (3, '机械臂总体图', 'https://wx1.sinaimg.cn/mw690/005Sy8A1ly1hdz7gr4umpj324e1g3af7.jpg', 'https://www.alsidt.com/public/uploads/20220331/0d63c80486b1d0178095120a998236d2.png', 'https://www.alsidt.com/public/uploads/20201003/554effd7bc5ba0c43b2d4ace0e86eaa5.jpg', NULL, NULL, 1, '成都观光电梯设计案例展示', '2020-10-03', 0);
INSERT INTO `t_type_details` VALUES (4, '收纳仓正面图', 'https://wx4.sinaimg.cn/mw690/005Sy8A1ly1hdz7e9tsfoj313n1g3gvb.jpg', 'https://www.alsidt.com/public/uploads/20201003/42835265b591791c3f5e15917f437763.jpg', NULL, NULL, NULL, 2, '成都雅居乐花园安装家用电梯', NULL, 0);
INSERT INTO `t_type_details` VALUES (5, '收纳仓数据图', 'https://wx4.sinaimg.cn/mw690/005Sy8A1ly1hdz7jxnvalj31281g3tfo.jpg', NULL, NULL, NULL, NULL, 2, '成都雅居乐花园安装家用电梯', NULL, 0);
INSERT INTO `t_type_details` VALUES (6, '抓夹正面图', 'https://wx4.sinaimg.cn/mw690/005Sy8A1ly1hdz7luyrd2j310e1g3gs2.jpg', 'https://www.alsidt.com/public/uploads/20201003/ead8f6adb992833dc6f6b7bf8f1bcdad.jpg', NULL, NULL, NULL, 3, '成都蔚蓝卡地亚家用别墅电梯成都蔚蓝卡地亚家用别墅电梯', NULL, 0);
INSERT INTO `t_type_details` VALUES (7, '抓夹数据图', 'https://wx4.sinaimg.cn/mw690/005Sy8A1ly1hdz7luqzi9j318h1g30xj.jpg', NULL, NULL, NULL, NULL, 3, NULL, NULL, 0);

SET FOREIGN_KEY_CHECKS = 1;
